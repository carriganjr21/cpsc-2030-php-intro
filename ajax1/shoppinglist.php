<?php
    session_start();
    header( "Content-type: application/json");

    $isLogin = isset($_SESSION['login'])? (bool)$_SESSION['login'] : false;

    $link = mysqli_connect( 'localhost', 'root', 'elbaron1' );
    if ( ! $link ) {
      $error_number = mysqli_connect_errno();
      $error_message = mysqli_connect_error();
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    $dbName = 'cars_database';
    if ( ! mysqli_select_db( $link, $dbName ) ) {
      $error_number = mysqli_errno( $link );
      $error_message = mysqli_error( $link );
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    switch ( $_SERVER['REQUEST_METHOD']) {
        case 'GET':
            if(isset($_REQUEST['logout'])) {
                $_SESSION['login'] = false;
                answer('OK', [
                    'login' => false,
                ]);
            }

            $results = getCarCollection($link);
            
            if ( ! $results ) {
                answer('ERROR', [], 'Cant fetch the cars collection');
            } else {

                answer('OK', [
                    'cars' => $results,
                    'login' => isset($_SESSION['login'])? $_SESSION['login'] : false,
                    'username' => $isLogin? 'Admin' : '',
                ]);
            }
            break;
        
        case 'POST':
            $loginAction = isset($_REQUEST["username"])? true : false ;

            if($loginAction) {
                if(isset($_SESSION['login']) && $_SESSION['login']) {
                    answer('ERROR', [], 'You already loged in.');
                }
                $username = $_REQUEST["username"];
                $password = $_REQUEST["password"];
                if($username == 'admin' && $password == "test") {
                    $_SESSION['login'] = true;
                    answer('OK', ['username'=>$username]);
                }
                answer('ERROR', [], 'Wrong username or password.');
            }

            $safe_model = mysqli_real_escape_string( $link, $_REQUEST["model"] );
            $safe_make = mysqli_real_escape_string( $link, $_REQUEST["make"] );
            $safe_mileage = mysqli_real_escape_string( $link, $_REQUEST["mileage"] );
            $safe_year = mysqli_real_escape_string( $link, $_REQUEST["year"] );
            if ( strlen( $safe_model ) <= 0 ||
                 strlen( $safe_model ) > 80 ||
                 strlen( $safe_mileage ) <= 0 ||
                 strlen( $safe_year ) <= 0 ||
                 strlen($safe_make) <= 0
            ) {
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );

                answer('ERROR', [], 'Wrong data.');
            }

            $query = "INSERT INTO cars ( make, model, mileage, year ) VALUES ( '$safe_make', '$safe_model', '$safe_mileage', '$safe_year' )";
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                answer('ERROR', [], 'Cant put new row to the cars table.');
            }
            answer('OK', [
                'id' => mysqli_insert_id($link),
                'model' =>  $_REQUEST["model"],
                'make' =>  $_REQUEST["make"],
                'mileage' =>  $_REQUEST["mileage"],
                'year' =>  $_REQUEST["year"],
            ]);
            break;
        
        case 'DELETE':
            if(!$isLogin) {
                answer('ERROR', [], 'You have not permission.');
            }
            $id = (int)$_REQUEST['id'];
            $query = "DELETE FROM cars WHERE id = " . $id;
            if ( ! mysqli_query( $link, $query ) ) {
                answer('ERROR', [], 'Cant to delete element ' . $id);
            }
            answer('OK', [
                'id' => $id,
            ]);
            break;

    }

    function getCarCollection($link) {
        $results = mysqli_query( $link, 'select * FROM cars' );
        $shoppinglist = array();

        if ( ! $results ) {
            $error_number = mysqli_errno( $link );
            $error_message = mysqli_error( $link );
            file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
            return false;
        }

        while( $record = mysqli_fetch_assoc( $results ) ) {
            $shoppinglist[] = $record;
        }

        mysqli_free_result( $results );

        return $shoppinglist;

    }

    function answer($status, $data, $error='') {
        $answer = [
            'status' => $status,
            'data' => $data,
            'error' => $error,
        ];
        echo json_encode( $answer );
        exit(1);
    }