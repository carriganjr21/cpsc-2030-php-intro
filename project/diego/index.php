
<?php get_header(); ?>
    <div class="content-area">
        <?php get_sidebar(); ?>
        <main id="main" class="site-main">

            <?php
            if ( have_posts() ) {

                // Load posts loop.
                while ( have_posts() ) {
                    the_post();
                    get_template_part( 'template-parts/list-of-posts' );
                }


            } else {

                // If no content, include the "No posts found" template.
                get_template_part( 'template-parts/single-post' );

            }
            ?>

        </main>
    </div>

<?php get_footer(); ?>

