<?php

if ( ! function_exists( 'diego_setup' ) ) :


endif;
add_filter('show_admin_bar', '__return_false');

function wpb_hook_javascript() {
    global $pagename;
    // HERE WE CHECK IF THE PAGE HAS NAME GALLERY, THEN IT WILL LOAD THIS SCRIPT
    if ($pagename == 'gallery') {
        ?>
        <!--this script will load if it matches-->
        <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/gallery.js"></script>
        <?php
    }
}
add_action('wp_head', 'wpb_hook_javascript');