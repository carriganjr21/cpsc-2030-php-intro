
<?php get_header(); ?>
<div class="content-area">
    <?php get_sidebar(); ?>
    <main id="main" class="site-main">

<!--        <h1>SINGLE</h1>-->
        <?php
        while ( have_posts() ) :
            the_post();
            get_template_part( 'template-parts/article', 'page' );
        endwhile;
        ?>
    </main>
</div>

<?php get_footer(); ?>
