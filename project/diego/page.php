<?php get_header(); ?>
<div class="content-area">
    <?php get_sidebar(); ?>
    <main id="main" class="site-main">

<!--        <h1>PAGE</h1>-->
        <?php
        while ( have_posts() ) :
            the_post();
            get_template_part( 'template-parts/single-post', 'page' );
        endwhile;
        ?>
    </main>
</div>

<?php get_footer(); ?>
