<article id="single"  <?php post_class(); ?>>
    <header class="entry-header">
        <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
    </header>


    <div class="entry-content">
        <?php
        /* translators: %s: Name of current post */
        the_content(
            sprintf(
                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'diego' ),
                get_the_title()
            )
        );

        the_posts_pagination(
            array(
                'prev_text'          => __( 'Previous page', 'twentysixteen' ),
                'next_text'          => __( 'Next page', 'twentysixteen' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
            )
        );

        ?>
    </div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->