<article id="single" <?php post_class(); ?>>
    <header class="entry-header">
        <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
    </header>


    <div class="entry-content">
        <?php
        /* translators: %s: Name of current post */
        the_content(
            sprintf(
                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'diego' ),
                get_the_title()
            )
        );

        wp_link_pages(
            array(
                'before' => '<div class="page-links">' . __( 'Pages:', 'diego' ),
                'after'  => '</div>',
            )
        );
        ?>
    </div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->