
    <aside id="sidebar" class="sidebar widget-area">
        <div class="mobile-menu-icon-close">Close</div>
        <div class="logo">
            <div class="image"><a href="<?php echo get_site_url(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/big-logo500.png" alt="Logo" /></a></div>
            <a href="<?php echo get_site_url(); ?>"><span>DiegoSite</span></a>
        </div>
        <nav>
<!--            <a href="/about-us">About Us</a>-->
<!--            <a href="/category/articles/">Articles</a>-->
<!--            <a href="/gallery">Gallery</a>-->
<!--            <a href="/contact-us">Contact Us</a>-->
<!--            <a href="/documentation">Documentation</a>-->
            <?php wp_nav_menu() ?>
        </nav>
    </aside>