<?php get_header(); ?>
<div class="main-page">
    <div class="main-image">
        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/big-logo1024.png" alt="Whistler" />
        <div class="description">
            <a href="<?php echo get_site_url(); ?>/about-us">let's find out <br/>what is <br/>Wistler </a>
        </div>
    </div>
</div>

<?php get_footer(); ?>
