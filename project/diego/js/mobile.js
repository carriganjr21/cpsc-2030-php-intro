$(function(){
    if($(".mobile-menu-icon").is(":visible")) {
        $(".mobile-menu-icon").on('click', function(){
            if($('aside').hasClass("open")) {
                $('aside').removeClass("open");
            }
            else {
                $('aside').addClass("open");
            }
        });
        $(".mobile-menu-icon-close").on('click', function(){
            if($('aside').hasClass("open")) {
                $('aside').removeClass("open");
            }
            else {
                $('aside').addClass("open");
            }
        })
    }
});