$(function(){
    DiegoGallery = {
        images: [],
        attributeName: "gallery-id",
        galleryId: "gallery-wrapper",
        current: 0,

        addImage: function(image) {
            var count = this.images.length;
            var srcsetList = image.attr("srcset").split(",");
            var src = "";
            for(var i = 0; i < srcsetList.length; i++) {
                var pair = srcsetList[i].split(" ");
                console.log(pair[1].replace(/ /g,""));
                if(pair[1].replace(/ /g,"") == "1024w") {
                    this.images.push(pair[0]);
                    image.attr(this.attributeName, count);
                    break;
                }
            }
        },

        show: function(event) {
            this.current = event.target.getAttribute(this.attributeName);
            this.init();
            this.showCurrent(event);
        },

        showCurrent: function(event) {
            event.stopPropagation();
            event.preventDefault();
            var html = this.template().replace('{src}', this.images[this.current]);
            $("#"+this.galleryId).html(html);
            $("#"+this.galleryId).show();
            $("#gallery_left").on("click", function(event){
                DiegoGallery.left(event);
            });
            $("#gallery_right").on("click", function(event){
                DiegoGallery.right(event);
            });
            console.log("show current");
        },

        left: function(event) {
            if(this.current > 0) {
                this.current--;
                this.showCurrent(event);
            }
        },

        right: function(event) {
            if(this.current < this.images.length-1) {
                this.current++;
                this.showCurrent(event);
            }
        },

        close: function() {
            if($("#"+this.galleryId).is(":visible")) {
                $("#" + this.galleryId).hide();
                console.log("close");
            }
        },

        template: function(image) {
            return ''+
                '<div class="gallery">'+
                    '<div class="left" id="gallery_left"><span>&lt;</span></div>'+
                    '<div class="center"><img src="{src}"></div>'+
                    '<div class="right" id="gallery_right"><span>&gt;</span></div>'+
                '</div>'
        },

        init: function() {
            if($("#"+this.galleryId).length < 1) {
                $("body").append('<div id="'+this.galleryId+'"></div>');
                $("body").on("click", function() {
                    DiegoGallery.close();
                })
            }
        }
    }
    $("#main img").each(function(){
        DiegoGallery.addImage($(this));
        $(this).on("click", function(event) {
            DiegoGallery.show(event);
        })
    });
});