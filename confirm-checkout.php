<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">


    <!-- Bootstrap CSS -->
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="form-validation.css" rel="stylesheet">

</head>
  <body>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col" colspan="3">Order Summary</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Name:</th>
      <td><?php echo $_REQUEST["firstName"]." ".$_REQUEST["lastName"]; ?></td>
    </tr>
    <tr>
      <th scope="row">Email</th>
     <td><?php echo $_REQUEST["email"]; ?></td>
    </tr>
    <tr>
      <th scope="row">Shipping Address:</th>
     <td><?php echo $_REQUEST["address"]; ?></td>
    </tr>
     <tr>
      <th scope="row">Country</th>
      <td><?php echo $_REQUEST["country"]; ?></td>
    </tr>
     <tr>
      <th scope="row">Province</th>
      <td><?php echo $_REQUEST["province"];?></td>
    </tr>
     <tr>
      <th scope="row">Postal Code</th>
      <td><? echo $_REQUEST["zip"]; ?></td>
    </tr> 
    <tr>
      <th scope="row">Shipping Information</th>
      <td><?php echo $_REQUEST["shippingMethod"]; ?></td>
    </tr> 
    <tr>
      <th scope="row">Credit Card</th>
      <td><?php echo $_REQUEST["cc-name"]; ?></td>
    </tr>
     <tr>
      <th scope="row">Credit Card Number/th>
      <td><?php echo $_REQUEST["cc-number"]; ?></td>
    </tr>
     <tr>
      <th scope="row">Credit Card Expiration</th>
      <td><?php echo $_REQUEST["cc-expiration"]; ?></td>
    </tr>
  </tbody>
</table>








<!-- HEAD OF PHP VARS ETC-->
      <?php 
         //Product Prices
      $price1=49.50;
      $price2=37.77;
      $price3=35;
      $tax1=0.05;
      $tax2=0.06;
      $tax3=0.15;
      $tax4=0.13;
      
      $productQty = $_REQUEST['quantityFirst'];
      $productQty1 = $_REQUEST['quantitySecond'];
      $productQty2 = $_REQUEST['quantityThird'];
      $brakeRow = "<br>";
      $state = $_REQUEST["state"]; 
      $declinedErrorMs = "Your transaction was declined by the merchant bank. Please try another card. Or call your bank.";
      
      ?>
      
      <?php

      
    
      if(isset($_REQUEST['check']))
      {
        echo "Wrapped as Gift".$brakeRow;
      }
      
      $cost = ($productQty*$price1 + $productQty1*$price2 + $productQty2*$price3);
      
      if($state == "Alberta" || $state == "British Columbia"
      || $state == "Manitoba" || $state == "Northwest Territories" 
      || $state == "Quebec" || $state == "Yukon" ){
         $cost += $cost* $tax1;
      }
      else if($state == "Saskatchewan"){
         $cost += $cost*$tax2;
      }
      else if($state == "New Brunswick" 
      || $state == "Newfoundland and Labrador" 
      || $state == "Nova Scotia"
      || $state == "Prince Edward Island"){
        $cost += $cost*$tax3;
      }
      else{
        $cost += $cost*$tax4;
      }
      
      if(isset($_REQUEST['check']))
      {
        $cost += 2;
      }
      
      
      echo "GRAND TOTAL : ".$cost.$brakeRow.$brakeRow;
      
      if($cost > 750){

      echo  $declinedErrorMs;
      }
      else{
        echo "THANK YOU FOR PURCHASING WITH US.";
      }
      ?>
      </br></br>
      <a href="https://dfb0ff108c794ee997e926180b6e3e3a.vfs.cloud9.us-east-1.amazonaws.com/cpsc-2030-php-intro/checkout.html"><button type="button"  class="btn btn-primary btn-lg btn-block">Go back to checkout page</button></a>

      
   </body>
   
   </html>
